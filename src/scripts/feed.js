(function (window) {
	'use strict';

	SlackApp.FeedView = class FeedView {
		/**
		 * @constructor
		 * @description This class initiates the search request flow and renders the feed and is the entry point
		 * for the lightbox
		 */
		constructor (ele) {
			this.rootElement = document.querySelector(ele);
			this.api = new SlackApp.GoogleImageSearchRequest('kitten', this.onApiSuccess.bind(this)).fetch();
			this.bindEvents();

			return this;
		}

		/**
		 * @description bind click listener that launches lightbox instances
		 */
		bindEvents () {
			this.rootElement.addEventListener('click', this.onClick.bind(this));
			return this;
		}

		/**
		 * @description onClick we instantiate a new lightbox (or use an existing one)
		 */
		onClick (evt) {
			let target = evt.target;

			while (target.nodeName.toLowerCase() !== 'a') {
				target = target.parentElement;
			}

			if (target.target !== '_new') {
				// use an existing lightbox if it exists
				this.activeLightbox = this.activeLightbox || new SlackApp.LightboxView({
					collection: this.items,
					el: '.lightbox'
				});

				this.activeLightbox.setActive(target.dataset.idx).render();

				evt.preventDefault();
			}

			return this;
		}

		onApiSuccess (data) {
			this.items = data.items;
			this.render();
			return this;
		}

		render () {
			// use a fragment for performance
			let frag = document.createDocumentFragment();
			let tmp = document.createElement('div');

			// Remove the "now fetching..." string
			this.rootElement.innerHTML = '';

			this.items.forEach(function (item, idx) {
				// We'd need integration tests for these api payload structure
				let thumbSrc = item.link;
				let link = item.link;

				// Create a template from the API results
				tmp.innerHTML =
					`<div class='feed-item'>
						<a class="site-title" href='http://${item.displayLink}' target="_new">${item.displayLink}</a>
						<a class="image-title" href='${link}' data-idx="${idx}" target="_new">${item.title}</a>
						<a class="lightbox-link" href='${link}' data-idx="${idx}">
							<img src='${thumbSrc}'>
						</a>
						<a class="image-title" href='${link}' data-idx="${idx}" target="_new">${link}</a>
					</div>`;

				frag.appendChild(tmp.firstChild);
			});

			this.rootElement.appendChild(frag);
			return this;
		}
	};

} (window));
