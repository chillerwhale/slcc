(function () {
	'use strict';

	SlackApp.GoogleImageSearchRequest = class GoogleImageSearchRequest {
		/**
		 * @constructor
		 * @description This class implements the API interface for Google CSE.
		 */
		constructor (query, success) {
			this.params = {
				// our custom search engine
				cx: '014700358710448494405:xie79n0qpmu',
				// public key meant for browsers
				key: 'AIzaSyAs7BnvaOnvAOxl7zg0xXeLW_ZcBUiR1XI',
				q: query,
				searchType: 'image',
				num: 9,
				imgType: 'photo'
			};

			this.endpoint = 'https://www.googleapis.com/customsearch/v1';
			this.successFn = success;
			return this;
		}

		error (status) {
			console.error('xhr failed: ' + status);
		}

		/**
		 * @description Simple helper to construct the request URL
		 */
		url () {
			let queryParams = [];

			for (let key in this.params) {
				queryParams.push(key + '=' + this.params[key]);
			}

			queryParams = '?' + queryParams.join('&');

			return this.endpoint + queryParams;
		}

		/**
		 * @description Simple ajax implentation
		 */
		fetch () {
			let request = new XMLHttpRequest(),
				self = this;

			request.open('GET', this.url(), true);
			request.onreadystatechange = function () {
				var status = request.status,
					data;

				if (request.readyState === 4) {
					if (status === 200)  {
						data = JSON.parse(request.responseText);
						self.successFn(data);
					} else {
						self.errorFn(status);
					}
				}

			};
			request.send();

			return this;
		}
	};

}());
