(function () {
	'use strict';

	SlackApp.LightboxView = class LightboxView {

		/**
		 * @constructor
		 * @description This class setups the lightbox and it's controls
		 */
		constructor (params) {
			this.collection = params.collection;
			this.activeIndex = params.activeIndex || 0;
			this.el = document.querySelector(params.el);
			this.imgEl = this.el.querySelector('.full-image');
			// Set up a simple cache for already rendered items
			this.cache = {};

			this.bindEvents();

			return this;
		}

		bindEvents () {
			this.el.querySelector('.close')
				.addEventListener('click', this.closeLightbox.bind(this));

			this.el.querySelector('.prev')
				.addEventListener('click', this.changeSlide.bind(this));

			this.el.querySelector('.next')
				.addEventListener('click', this.changeSlide.bind(this));

			return this;
		}

		changeSlide (evt) {
			evt.preventDefault();
			let dir = evt.target.className;

			if (dir === 'left') {
				this.activeIndex--;
			} else {
				this.activeIndex++;
			}

			// Wrap the results so a user never hits the end of a collection
			if (this.activeIndex < 0) {
				this.activeIndex = this.collection.length - 1;
			}

			if (this.activeIndex >= this.collection.length) {
				this.activeIndex = 0;
			}

			this.render();
			return this;
		}

		/**
		 * @description We never actually destroy the lightbox, but rather, just hide it
		 */
		closeLightbox (evt) {
			evt.preventDefault();
			this.el.classList.remove('active');
			return this;
		}

		setActive (idx) {
			this.activeIndex = idx;
			return this;
		}

		/**
		 * @description For simplicity sake, we just clear the container on navigation or closing
		 */
		clear() {
			this.imgEl.innerHTML = '';
			return this;
		}

		render () {
			this.clear();

			// Check if element is in cache, if not, do the full render flow
			if (!this.cache[this.activeIndex]) {

				let imgSrc = this.collection[this.activeIndex].link,
					tmp = document.createElement('div');

				tmp.innerHTML = `<img src="${imgSrc}">`;

				tmp.firstChild.classList.add(`item-${this.activeIndex}`);
				// set item in cache
				this.cache[this.activeIndex] = tmp.firstChild;
				this.imgEl.appendChild(tmp.firstChild);

			} else {
				// image is in cache, so let's just show it
				this.imgEl.appendChild(this.cache[this.activeIndex]);
			}

			// render once everything is done
			this.el.classList.add('active');
			return this;
		}
	};

} ());

