'use strict';

var _classCallCheck = function (instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } };

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

(function () {
	'use strict';

	SlackApp.LightboxView = (function () {

		/**
   * @constructor
   * @description This class setups the lightbox and it's controls
   */

		function LightboxView(params) {
			_classCallCheck(this, LightboxView);

			this.collection = params.collection;
			this.activeIndex = params.activeIndex || 0;
			this.el = document.querySelector(params.el);
			this.imgEl = this.el.querySelector('.full-image');
			// Set up a simple cache for already rendered items
			this.cache = {};

			this.bindEvents();

			return this;
		}

		_createClass(LightboxView, [{
			key: 'bindEvents',
			value: function bindEvents() {
				this.el.querySelector('.close').addEventListener('click', this.closeLightbox.bind(this));

				this.el.querySelector('.prev').addEventListener('click', this.changeSlide.bind(this));

				this.el.querySelector('.next').addEventListener('click', this.changeSlide.bind(this));

				return this;
			}
		}, {
			key: 'changeSlide',
			value: function changeSlide(evt) {
				evt.preventDefault();
				var dir = evt.target.className;

				if (dir === 'left') {
					this.activeIndex--;
				} else {
					this.activeIndex++;
				}

				// Wrap the results so a user never hits the end of a collection
				if (this.activeIndex < 0) {
					this.activeIndex = this.collection.length - 1;
				}

				if (this.activeIndex >= this.collection.length) {
					this.activeIndex = 0;
				}

				this.render();
				return this;
			}
		}, {
			key: 'closeLightbox',

			/**
    * @description We never actually destroy the lightbox, but rather, just hide it
    */
			value: function closeLightbox(evt) {
				evt.preventDefault();
				this.el.classList.remove('active');
				return this;
			}
		}, {
			key: 'setActive',
			value: function setActive(idx) {
				this.activeIndex = idx;
				return this;
			}
		}, {
			key: 'clear',

			/**
    * @description For simplicity sake, we just clear the container on navigation or closing
    */
			value: function clear() {
				this.imgEl.innerHTML = '';
				return this;
			}
		}, {
			key: 'render',
			value: function render() {
				this.clear();

				// Check if element is in cache, if not, do the full render flow
				if (!this.cache[this.activeIndex]) {

					var imgSrc = this.collection[this.activeIndex].link,
					    tmp = document.createElement('div');

					tmp.innerHTML = '<img src="' + imgSrc + '">';

					tmp.firstChild.classList.add('item-' + this.activeIndex);
					// set item in cache
					this.cache[this.activeIndex] = tmp.firstChild;
					this.imgEl.appendChild(tmp.firstChild);
				} else {
					// image is in cache, so let's just show it
					this.imgEl.appendChild(this.cache[this.activeIndex]);
				}

				// render once everything is done
				this.el.classList.add('active');
				return this;
			}
		}]);

		return LightboxView;
	})();
})();