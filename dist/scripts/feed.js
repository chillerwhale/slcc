'use strict';

var _classCallCheck = function (instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } };

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

(function (window) {
	'use strict';

	SlackApp.FeedView = (function () {
		/**
   * @constructor
   * @description This class initiates the search request flow and renders the feed and is the entry point
   * for the lightbox
   */

		function FeedView(ele) {
			_classCallCheck(this, FeedView);

			this.rootElement = document.querySelector(ele);
			this.api = new SlackApp.GoogleImageSearchRequest('kitten', this.onApiSuccess.bind(this)).fetch();
			this.bindEvents();

			return this;
		}

		_createClass(FeedView, [{
			key: 'bindEvents',

			/**
    * @description bind click listener that launches lightbox instances
    */
			value: function bindEvents() {
				this.rootElement.addEventListener('click', this.onClick.bind(this));
				return this;
			}
		}, {
			key: 'onClick',

			/**
    * @description onClick we instantiate a new lightbox (or use an existing one)
    */
			value: function onClick(evt) {
				var target = evt.target;

				while (target.nodeName.toLowerCase() !== 'a') {
					target = target.parentElement;
				}

				if (target.target !== '_new') {
					// use an existing lightbox if it exists
					this.activeLightbox = this.activeLightbox || new SlackApp.LightboxView({
						collection: this.items,
						el: '.lightbox'
					});

					this.activeLightbox.setActive(target.dataset.idx).render();

					evt.preventDefault();
				}

				return this;
			}
		}, {
			key: 'onApiSuccess',
			value: function onApiSuccess(data) {
				this.items = data.items;
				this.render();
				return this;
			}
		}, {
			key: 'render',
			value: function render() {
				// use a fragment for performance
				var frag = document.createDocumentFragment();
				var tmp = document.createElement('div');

				// Remove the "now fetching..." string
				this.rootElement.innerHTML = '';

				this.items.forEach(function (item, idx) {
					// We'd need integration tests for these api payload structure
					var thumbSrc = item.link;
					var link = item.link;

					// Create a template from the API results
					tmp.innerHTML = '<div class=\'feed-item\'>\n\t\t\t\t\t\t<a class="site-title" href=\'http://' + item.displayLink + '\' target="_new">' + item.displayLink + '</a>\n\t\t\t\t\t\t<a class="image-title" href=\'' + link + '\' data-idx="' + idx + '" target="_new">' + item.title + '</a>\n\t\t\t\t\t\t<a class="lightbox-link" href=\'' + link + '\' data-idx="' + idx + '">\n\t\t\t\t\t\t\t<img src=\'' + thumbSrc + '\'>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t\t<a class="image-title" href=\'' + link + '\' data-idx="' + idx + '" target="_new">' + link + '</a>\n\t\t\t\t\t</div>';

					frag.appendChild(tmp.firstChild);
				});

				this.rootElement.appendChild(frag);
				return this;
			}
		}]);

		return FeedView;
	})();
})(window);