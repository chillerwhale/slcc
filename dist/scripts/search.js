'use strict';

var _classCallCheck = function (instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } };

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

(function () {
	'use strict';

	SlackApp.GoogleImageSearchRequest = (function () {
		/**
   * @constructor
   * @description This class implements the API interface for Google CSE.
   */

		function GoogleImageSearchRequest(query, success) {
			_classCallCheck(this, GoogleImageSearchRequest);

			this.params = {
				// our custom search engine
				cx: '014700358710448494405:xie79n0qpmu',
				// public key meant for browsers
				key: 'AIzaSyAs7BnvaOnvAOxl7zg0xXeLW_ZcBUiR1XI',
				q: query,
				searchType: 'image',
				num: 9,
				imgType: 'photo'
			};

			this.endpoint = 'https://www.googleapis.com/customsearch/v1';
			this.successFn = success;
			return this;
		}

		_createClass(GoogleImageSearchRequest, [{
			key: 'error',
			value: function error(status) {
				console.error('xhr failed: ' + status);
			}
		}, {
			key: 'url',

			/**
    * @description Simple helper to construct the request URL
    */
			value: function url() {
				var queryParams = [];

				for (var key in this.params) {
					queryParams.push(key + '=' + this.params[key]);
				}

				queryParams = '?' + queryParams.join('&');

				return this.endpoint + queryParams;
			}
		}, {
			key: 'fetch',

			/**
    * @description Simple ajax implentation
    */
			value: function fetch() {
				var request = new XMLHttpRequest(),
				    self = this;

				request.open('GET', this.url(), true);
				request.onreadystatechange = function () {
					var status = request.status,
					    data;

					if (request.readyState === 4) {
						if (status === 200) {
							data = JSON.parse(request.responseText);
							self.successFn(data);
						} else {
							self.errorFn(status);
						}
					}
				};
				request.send();

				return this;
			}
		}]);

		return GoogleImageSearchRequest;
	})();
})();