# Slack Coding Challenge

Hello Slack friends! For this coding challenge I used vanilla ES6 compiled with babel! You can find all of my source code in `/src`. 

## Accessing the project on Heroku
The live demo can be found on here: [https://stark-thicket-7981.herokuapp.com/](https://stark-thicket-7981.herokuapp.com/).

## Running on your local machine
* From the root of the project: `$ python -m SimpleHTTPServer 8888`
* Navigate to `http://localhost:8888` in your browser
Or simply open the `index.html`! :D

## Running the tests!
To run the tests, simply open `tests/test.html` in your browser.
