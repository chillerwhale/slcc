'use strict';
QUnit.module('GoogleImageSearchRequest', {});
QUnit.test('exists', function (assert) {
	assert.ok(window.SlackApp.GoogleImageSearchRequest, ' is defined!');
	assert.ok(typeof window.SlackApp.GoogleImageSearchRequest === 'function', ' is a class constructor!');
});
