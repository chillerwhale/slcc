'use strict';
QUnit.module('FeedView', {
	beforeEach: function () {
		var ele = document.createElement('div');
		ele.className = 'mock-feed';
		document.body.appendChild(ele);
	},
	afterEach: function () {
		var ele = document.querySelector('.mock-feed');
		ele.parentNode.removeChild(ele);
	}
});

QUnit.test('FeedView exists', function (assert) {
	assert.ok(SlackApp.FeedView, 'it exists!');
	assert.ok(typeof SlackApp.FeedView === 'function', 'is class constructor');
});

QUnit.test('FeedView instantiates correctly', function (assert) {
	var feed = new SlackApp.FeedView('.mock-feed');

	// This is a brittle test since the routine to fetch AJAX is automatic, this is a race
	assert.ok(!feed.rootElement.children.length, 'there are no items in the feed');
	assert.ok(!feed.items, 'there are no items in the collection');

	assert.ok(feed.rootElement.className === 'mock-feed', 'it bootstraps the right element');
	assert.ok(
		feed.api instanceof SlackApp.GoogleImageSearchRequest,
		'it instantiates the GoogleImageSearchRequest'
	);

	// Ghetto async/timeout test
	var done = assert.async();
	setTimeout(function () {
		assert.ok(feed.rootElement.children.length, 'there are items in the feed');
		assert.ok(feed.items.length, 'there are items in the collection');
		done();
	}, 3000);
});

