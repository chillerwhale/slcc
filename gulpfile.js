'use strict';
var gulp = require('gulp'),
	babel = require('gulp-babel');

gulp.task('default', ['scripts:compile']);

gulp.task('scripts:compile', function () {
	return gulp.src('src/**/*.js')
		.pipe(babel())
		.pipe(gulp.dest('dist'));
});

gulp.task('watch', ['default'], function () {
	gulp.watch('src/**/*', ['default']);
});
